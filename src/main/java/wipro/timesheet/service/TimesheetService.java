package wipro.timesheet.service;

import java.util.List;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.domain.Manager;
import wipro.timesheet.domain.Task;

public interface TimesheetService {

	/* 
	 * returns the busiest tasks (with most employees)..if empty return null
	 */
	Task busiestTask();
	
	/*
	 * return all tasks for an employee
	 */
	List<Task> tasksForEmployee(Employee e);
	
	/*
	 * returns all tasks for a manager
	 */
	List<Task> tasksforManager(Manager m);
	
}
