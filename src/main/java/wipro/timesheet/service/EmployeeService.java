package wipro.timesheet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.repository.EmployeeRepository;
import wipro.timesheet.repository.TaskRepository;
import wipro.timesheet.repository.TimesheetRepository;

@Service("employeeService")
public class EmployeeService {

	private EmployeeRepository employeeRepo;
	private TaskRepository taskRepo;
	private TimesheetRepository timesheetRepo;
	
	@Autowired
	public EmployeeService(EmployeeRepository employeeRepo, TaskRepository taskRepo,
			TimesheetRepository timesheetRepo) {
		super();
		this.employeeRepo = employeeRepo;
		this.taskRepo = taskRepo;
		this.timesheetRepo = timesheetRepo;
	}
	
	public List<Employee> getAllEmployees() {
		List<Employee> empList = new ArrayList<>();
		for(Employee e: employeeRepo.findAll()) {
			empList.add(e);
		}
		return empList;
	}
	
	public boolean removeEmployee(Employee e) {
		
		// check if emp is assigned any task
		
//		taskRepo.f
		
		if (! taskRepo.findAllByAssignedEmployees(e).isEmpty()) {
			return false;
		}
		
		if (! timesheetRepo.findByWho(e).isEmpty()) {
			return false;
		}
		
		employeeRepo.delete(e);
		return true;
	}
	
	public Long getEmployeeCount() {
//		employeeRepo.c
		return employeeRepo.count();
	}
	
	public Employee save(Employee employee) {
		return employeeRepo.save(employee);
	}

}
