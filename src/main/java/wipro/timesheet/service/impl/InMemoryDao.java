package wipro.timesheet.service.impl;

import java.util.ArrayList;
import java.util.List;

import wipro.timesheet.service.GenericDao;

public class InMemoryDao<E, K> implements GenericDao<E, K> {

	private List<E> entities = new ArrayList<E>();
	
	public void add(E entity) {
		entities.add(entity);
	}

	public void update(E entity) {
		throw new UnsupportedOperationException("Not available in demo version");
	}

	public void remove(E entity) {
		entities.remove(entity);
	}

	public E find(K key) {
		if(entities.isEmpty()) {
			return null;
		}
		return entities.get(0);
	}

	public List<E> list() {
		return entities;
	}
}
