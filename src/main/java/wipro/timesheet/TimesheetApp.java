package wipro.timesheet;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.service.EmployeeService;

@SpringBootApplication
public class TimesheetApp {
	
	private static final Logger log = LoggerFactory.getLogger(TimesheetApp.class);


	public static void main(String[] args) {
		SpringApplication.run(TimesheetApp.class, args);
	}
	
	@Bean
//	public CommandLineRunner demo(EmployeeRepository empRepo) {
	public CommandLineRunner demo(EmployeeService empService) {
		return (args) -> {
			
			log.info("CURRENT SIZE: " + empService.getEmployeeCount());
			empService.save(new Employee("Abhishek Danej", "AIMS"));
			log.info("STARTING...");
			for(Employee e: empService.getAllEmployees() ) {
				log.info(e.getName());
			}
			log.info("NEW SIZE: " + empService.getEmployeeCount());
			log.info("ENDING...");
		};
	}
}

