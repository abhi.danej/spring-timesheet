package wipro.timesheet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import wipro.timesheet.domain.Employee;

@Repository("employeeRepository")
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
