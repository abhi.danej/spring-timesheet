package wipro.timesheet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.domain.Timesheet;

@Repository("timesheetRepository")
public interface TimesheetRepository extends CrudRepository<Timesheet, Long> {
	
//	List<Timesheet> getAllTimesheetForEmployee(Long employee_id);
	List<Timesheet> findByWho(Employee employee);
}
