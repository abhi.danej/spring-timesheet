package wipro.timesheet.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import wipro.timesheet.domain.Manager;

@Repository("managerRepository")
public interface ManagerRepository extends CrudRepository<Manager, Long> {
	
	
}
