package wipro.timesheet.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.domain.Task;

@Repository("taskRepository")
public interface TaskRepository extends CrudRepository<Task, Long> {
	
//	List<Task> getTasksAssignedToEmployee(Long employee_id);
	List<Task> findAllByAssignedEmployees(Employee employee);
	
}
