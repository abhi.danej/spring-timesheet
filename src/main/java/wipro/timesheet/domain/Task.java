package wipro.timesheet.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import lombok.Data;

@Data
@Entity
@Table(name="task")
public class Task {

	public Task(String description, Manager manager, Employee...employees) {
		assignedEmployees.addAll(Arrays.asList(employees));
		this.manager = manager;
		this.description = description;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
			name="task_employee",
			joinColumns = {@JoinColumn(name="task_id")},
			inverseJoinColumns = {@JoinColumn(name="employee_id")}
	)
	private List<Employee> assignedEmployees = new ArrayList<Employee>();
	
	@OneToOne
	@JoinColumn(name="manager_id")
	private Manager manager;
	
	private boolean completed;
	private String description;
	
	public void removeEmployee(Employee e) {
		assignedEmployees.remove(e);
	}
	
	public void addEmployee(Employee e) {
		assignedEmployees.add(e);
	}
	
}
