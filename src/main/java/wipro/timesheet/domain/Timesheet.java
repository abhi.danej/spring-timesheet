package wipro.timesheet.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="timesheet")
public class Timesheet {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne
	@JoinColumn(name="employee_id")
	private Employee who;
	
	
	@OneToOne
	@JoinColumn(name="task_id")
	private Task task;
	
	private Integer hours;
	
	public void alterHours(Integer hours) {
		this.hours = hours;
	}
}
