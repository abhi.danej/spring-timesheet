package wipro.timesheet.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.service.impl.InMemoryDao;

public class EmployeeDaoTest {
	
	private GenericDao<Employee, Long> employeeDao = new InMemoryDao<Employee, Long>();
	
	@Before
	public void setUp() {
		for(int i=0; i<5; i++) {
			Employee e = new Employee("Abhishek" + i, "IT");
			employeeDao.add(e);
		}
	}

	@Test
	public void testAdd() {
		int oldSize = employeeDao.list().size();
		employeeDao.add(new Employee("Ashwin", "IT"));
		int newSize = employeeDao.list().size();
		assertFalse(newSize == oldSize);
		
	}

	@Test
	public void testUpdate() {
		// TODO after implementation
	}

	@Test
	public void testRemove() {
		int oldSize = employeeDao.list().size();
		Employee e = employeeDao.find(1L);
		employeeDao.remove(e);
		int newSize = employeeDao.list().size();
		assertFalse(newSize == oldSize);
	}

	@Test
	public void testList() {
		java.util.List<Employee> list = employeeDao.list();
		assertNotNull(list);
		assertFalse(list.isEmpty());
	}

}
