package wipro.timesheet.repository;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration()
public class EmployeeRepositoryTest {

	EmployeeRepository employeeRepo;
	private final String deleteSQL = "src/main/resources/cleanup.sql";

	@Autowired
	JdbcTemplate jdbc = null;
	
	@Before
	public void setUp() throws Exception {
		
		String sqls = String.join(" ", Files.readAllLines(new File(deleteSQL).toPath()));
		System.out.println("inside before: " + sqls);
		jdbc.execute("select from task_employee");
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
