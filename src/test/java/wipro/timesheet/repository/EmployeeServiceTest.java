package wipro.timesheet.repository;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import wipro.timesheet.domain.Employee;
import wipro.timesheet.service.EmployeeService;

/*below 2 lines are important to run the test case */
@SpringBootTest
@RunWith(SpringRunner.class) 
public class EmployeeServiceTest {
	
	private EmployeeService employeeService;
	private TaskRepository taskRepo;
	private EmployeeRepository employeeRepo;
	private TimesheetRepository timesheetRepo;
	private Employee emp;

	@Before
	public void setUp() throws Exception {
		
		employeeService = new EmployeeService(employeeRepo, taskRepo, timesheetRepo);
		System.out.println("New employeeService object created.");
	}

	@Test
	public void testRemoveEmployee() {
		
		if(employeeService == null) {
			System.out.println("employee service is null");
		}
		
		System.out.println("inside TEST.");
//		Long origSize = employeeService.getEmployeeCount();

		System.out.println("inside TEST2.");
//		emp = new Employee("Abhishek Danej", "AIMS");
		emp = Employee.builder().id(8L).department("AIMS").name("Abhishek Danej").build();
		Employee e = employeeService.save(emp);
		if(e == null) {
			System.out.println("employee is null");
		}
		System.out.println("Added employee: " + e.getName());
		
		Long newSize = employeeService.getEmployeeCount();
		System.out.println("New size: " + newSize);
		assertTrue(0L < newSize);
	}

}
